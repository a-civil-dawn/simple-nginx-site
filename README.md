# Simple NGINX Site

This repository serves as a template for creating containers for simple sites served by NGINX. This includes essential files, scripts, and configurations. It is designed to help you kickstart your website with ease.

## Table of Contents

- [Overview](#overview)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Usage](#usage)
- [Versioning](#versioning)
- [Customization](#customization)
- [Contributing](#contributing)
- [License](#license)

## Overview

This repository provides a structure and essential files for creating Docker containers for your website. It includes:

- docker-compose.yml file for orchestrating your containers.
- .env file template for storing environment variables (in the docker-compose.yml file).
- A nightly.sh script for automated backups and regular restarts (create a cron job to run it).
- A directory of scripts containing:
  - An entrypoint script for running startup tasks.
  - A start script for adding complicated startup logic.
  - A stop script to handle SIGTERM signals gracefully.

## Getting Started

### Prerequisites

Before using this template, ensure you have the following prerequisites installed:

- Docker: [Install Docker](https://docs.docker.com/get-docker/)
- Docker Compose: [Install Docker Compose](https://docs.docker.com/compose/install/)

### Usage

1. Fork and clone this repository to start your project:
    ```
    git clone git@gitlab.com:a-civil-dawn/simple-nginx-site.git
    mv container your-docker-project
    cd your-docker-project
    ```
2. Customize the following files as per your project requirements:
  - .env: Create the .env from the template in the compose file and configure environment variables.
  - website code
    - Select your location (DATA_LOC) on local disk where your docker volumes will mount then add the following directories and files
      - templates (nginx templates like specified in https://devopsian.net/p/nginx-config-template-with-environment-vars/)
      - html (your site goes here)
      - images (self-hosted images - note these should not be stored in git repos)
      - logs (whatever is created in /var/log/nginx)
3. Build and run your Docker containers:
    `docker-compose up -d`
4. Your application is now running inside Docker containers!
5. (optional) Add a cron job with the configuration commented in the nightly.sh script in order to run the nightly resets
6. (optional) Check out the following simple website projects which are compatible with this server:
  - https://gitlab.com/licham.io/templates/simple-single-page-site
  - https://gitlab.com/licham.io/templates/simple-multi-page-site

## Versioning

The version file in this directory should be updated when changes are made to the git repo of this and any dependency projects. It will show version numbers for each.

## Customization

Feel free to customize and extend this template to meet your specific project requirements. You can add additional scripts, configuration files, or Docker-related files as needed.

## Contributing

Contributions are welcome! If you have any improvements or suggestions for this template, please open an issue or submit a pull request.

## License

This template is licensed under the MIT License.