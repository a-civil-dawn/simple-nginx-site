#!/bin/bash

# Run this with sudo crontab -e
# Replace xx with the hour of the day you want to run this
# 0 xx * * * bash /absolute_path_to_nightly.sh

# Set this cronjob's directory to the directory of the nightly.sh file for environment variables and docker-compose
cd `dirname $0`

# Aource the environment variable file
source .env

# Atop the server and clean up orphaned/anonymous images, networks, containers, and volumes
docker-compose down --rmi all --remove-orphans

# Copy the entire project volumes directory
mkdir -p "${BACKUP_LOC}/${ENVIRONMENT:-development}"
cp -Rp "${DATA_LOC}/${ENVIRONMENT:-development}" "${BACKUP_LOC}/${ENVIRONMENT:-development}"_`date '+%Y_%m_%d__%H_%M_%S'`

# The config directory should be a git repo hosted in your preferred git site and doesn't need to be backed up here

# Restart and rebuild in case there are changes to the Dockerfile/docker-compose.yml
docker-compose up -d --build --force-recreate

# Change to the directory holding the old saves so that the delete script doesn't have to look elsewhere
cd "${BACKUP_LOC}/${ENVIRONMENT:-development}"

# Remove all but 8 most recent files and directories (last 7 days)
ls -tp | grep -v '^d' | tail -n +9 | xargs -I {} rm -rf -- {}
