#!/bin/bash

echo "Preparing NGINX config files"
sh /docker-entrypoint.d/20-envsubst-on-templates.sh
echo "Starting the service"
rm /etc/nginx/nginx.conf
mv /etc/nginx/conf.d/nginx.conf /etc/nginx/nginx.conf
nginx -g 'daemon off;'