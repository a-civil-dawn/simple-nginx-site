#!/bin/bash

# This script will run inside the container to start and manage the server

# For more information on these crazy functions, see https://unix.stackexchange.com/questions/146756/forward-sigterm-to-child-in-bash/444676#444676
# start.sh and stop.sh are in the local directory to have more complicated start logic in a dedicated place and more graceful shutdown logic, too
# Should the shut.sh fail a SIGTERM will be sent to the pid to fully stop it
# The trap in prep_term changes the TERM behaviour until the trap in wait_term resets it after the service is closed)
prep_term()
{
    unset term_child_pid
    unset term_kill_needed
    trap 'handle_term' TERM INT
}

handle_term()
{
    if [ "${term_child_pid}" ]; then
      ./stop.sh "${term_child_pid}"
      kill -TERM "${term_child_pid}" 2>/dev/null
    else
      term_kill_needed="yes"
    fi
}

wait_term()
{
  term_child_pid=$!
    if [ "${term_kill_needed}" ]; then
      ./stop.sh "${term_child_pid}"
      kill -TERM "${term_child_pid}" 2>/dev/null
    fi
    wait ${term_child_pid} 2>/dev/null
    trap - TERM INT
    wait ${term_child_pid} 2>/dev/null
}

pre_flight()
{
    echo "prepping to start the service"
}

# The below uses the functions from above to trap SIGTERM and shut down gracefully when the wait_term has a chance to start
# Otherwise bash might ignore the signal while starting the service or mess up any prep in the pre_flight which may be crucial for future operation
# Feel free to put other important pre-start logic in pre_flight
pre_flight &
prep_term

# The below starts the service
# The & puts the server in the background so that the wrapper can start waiting for SIGTERM
sh /scripts/start.sh &
wait_term
